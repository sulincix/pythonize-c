#!/usr/bin/python3
import sys
import os
CFLAGS=[]
file_list=[]
def transform(source):
    target=open(source+".c","w")
    source=open(source,"r")
    tabnum=0
    lastlinetab=0
    line="\n"
    while "\n" in line:
        line=source.readline()
        if line.strip().find("import ") == 0:
            line=line.replace("import ","#include ")
        is_func=False
        if len(line) > 1 and  ":\n" in line :
            line=line.replace(":\n","{\n")
            is_func=True
        if len(line.strip()) > 0 and line.strip()[0] == "#":
            is_func=True
        linetab=0
        for c in line:
            if c==" " or c=="\t":
                linetab+=1
            else:
                break
        if lastlinetab > linetab:
            target.write((linetab*" ")+"}\n")
            tabnum-=1
        elif lastlinetab < linetab:
            tabnum+=1
        lastlinetab = linetab
        target.write(linetab*" "+line.strip())
        if not is_func:
            target.write(";")
        target.write("\n")
              
for i in sys.argv[1:]:
    if(os.path.isfile(i)):
        transform(i)
        file_list.append(i+".c")
        CFLAGS.append(i+".c")
    else:
        CFLAGS.append(i)
    
cmd="gcc"
for i in CFLAGS:
    cmd+=" "+i

if os.environ.get("CFLAGS"):
    cmd+=" "+str(os.environ.get("CFLAGS"))
    
os.system(cmd)
if str(os.environ.get("KEEP_CY")).lower()== "true":
    exit(0)
for i in file_list:
    os.system("rm -f "+i)
